# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(unique=True, max_length=80)
    class Meta:
        managed = False
        db_table = 'auth_group'

class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    group_id = models.IntegerField()
    permission = models.ForeignKey('AuthPermission')
    class Meta:
        managed = False
        db_table = 'auth_group_permissions'

class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    content_type_id = models.IntegerField()
    codename = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'auth_permission'

class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'auth_user'

class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    group = models.ForeignKey(AuthGroup)
    class Meta:
        managed = False
        db_table = 'auth_user_groups'

class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    permission = models.ForeignKey(AuthPermission)
    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'

class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)
    action_time = models.DateTimeField()
    user_id = models.IntegerField()
    content_type_id = models.IntegerField(blank=True, null=True)
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    class Meta:
        managed = False
        db_table = 'django_admin_log'

class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'django_content_type'

class DjangoSession(models.Model):
    session_key = models.CharField(unique=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'django_session'

class MainChargerequest(models.Model):
    id = models.IntegerField(primary_key=True)
    msisdn = models.CharField(max_length=100)
    parameters = models.CharField(max_length=250)
    result_code = models.CharField(max_length=20)
    responce_body = models.TextField()
    create = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'main_chargerequest'

class MainMobiledevices(models.Model):
    id = models.IntegerField(primary_key=True)
    msisdn = models.CharField(max_length=100)
    tarif = models.CharField(max_length=100)
    create = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'main_mobiledevices'

class SouthMigrationhistory(models.Model):
    id = models.IntegerField(primary_key=True)
    app_name = models.CharField(max_length=255)
    migration = models.CharField(max_length=255)
    applied = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'south_migrationhistory'

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Channels(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45, blank=True)
    country = models.CharField(max_length=45, blank=True)
    stream = models.CharField(max_length=45, blank=True)
    application = models.CharField(max_length=45, blank=True)
    server = models.CharField(max_length=45, blank=True)
    logo = models.CharField(max_length=200, blank=True)
    screenshot = models.CharField(max_length=200)
    is_free = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'channels'

class Packages(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45, blank=True)
    logo = models.CharField(max_length=200, blank=True)
    description = models.CharField(max_length=300, blank=True)
    price = models.FloatField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    enabled = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'packages'

class PackagesChannels(models.Model):
    package = models.ForeignKey(Packages)
    channel = models.ForeignKey(Channels)
    class Meta:
        managed = False
        db_table = 'packages_channels'

class PaymentTransactions(models.Model):
    id = models.IntegerField(primary_key=True)
    transaction_id = models.CharField(max_length=100)
    user = models.ForeignKey('Users', blank=True, null=True)
    package = models.ForeignKey(Packages, blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    op_transaction_id = models.CharField(max_length=45, blank=True)
    status_code = models.CharField(max_length=8, blank=True)
    status_description = models.CharField(max_length=200, blank=True)
    amount = models.FloatField(blank=True, null=True)
    type = models.CharField(max_length=20, blank=True)
    class Meta:
        managed = False
        db_table = 'payment_transactions'

class ProgramManager(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45, blank=True)
    login = models.CharField(max_length=45, blank=True)
    password = models.CharField(max_length=45, blank=True)
    class Meta:
        managed = False
        db_table = 'program_manager'

class ProgramManagerChannels(models.Model):
    program_m = models.ForeignKey(ProgramManager)
    channel = models.ForeignKey(Channels)
    class Meta:
        managed = False
        db_table = 'program_manager_channels'

class Programs(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=200, blank=True)
    channel = models.ForeignKey(Channels, blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'programs'

class TvSchedule(models.Model):
    id = models.IntegerField(primary_key=True)
    program = models.ForeignKey(Programs, blank=True, null=True)
    begin_t = models.DateTimeField(blank=True, null=True)
    end_t = models.DateTimeField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'tv_schedule'

class Users(models.Model):
    id = models.IntegerField(primary_key=True)
    uid = models.CharField(unique=True, max_length=200, blank=True)
    msisdn = models.CharField(unique=True, max_length=20)
    subscription_expire_date = models.DateTimeField(blank=True, null=True)
    auto_subscribe = models.BooleanField()
    class Meta:
        managed = False
        db_table = 'users'

class UsersPackages(models.Model):
    user = models.ForeignKey(Users)
    package = models.ForeignKey(Packages)
    auto_subscribe = models.BooleanField()
    subscription_date = models.DateTimeField(blank=True, null=True)
    expire_date = models.DateTimeField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'users_packages'

