from django.contrib import admin
from main.models import Channels, Packages, TvSchedule, Programs, Users, PaymentTransactions, UsersPackages, Tarif

# Register your models here.

#class MobileDevicesAdmin(admin.ModelAdmin):
#    list_display = ('msisdn', 'tarif', 'create')
#    list_filter = ('create',)
#    search_fields = ('msisdn',)
#admin.site.register(MobileDevices, MobileDevicesAdmin)


#class ChargeRequestAdmin(admin.ModelAdmin):
#    list_display = ('msisdn', 'result_code', 'create')
#    list_filter = ('create','result_code')
#    search_fields = ('msisdn',)
#admin.site.register(ChargeRequest, ChargeRequestAdmin)


class ChannelsAdmin(admin.ModelAdmin):
    list_display = ('name', 'stream', 'country', 'thumb')
    filter_horizontal = ('programs',)
admin.site.register(Channels, ChannelsAdmin)

#class TvScheduleAdmin(admin.ModelAdmin):
#    list_display = ('program', 'begin_t', 'end_t')
#admin.site.register(TvSchedule, TvScheduleAdmin)


class ProgramsAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'begin_t', 'end_t')
admin.site.register(Programs, ProgramsAdmin)


class UsersAdmin(admin.ModelAdmin):
    list_display = ('uid', 'msisdn', 'subscription_expire_date', 'auto_subscribe')
    list_filter = ('subscription_expire_date',)
    search_fields = ('uid', 'msisdn')
admin.site.register(Users, UsersAdmin)


class TarifAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')
#    filter_horizontal = ('channels',)
admin.site.register(Tarif, TarifAdmin)

class PackagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'tarif', 'enabled', 'thumb')
#    filter_horizontal = ('channels',)
admin.site.register(Packages, PackagesAdmin)




class PaymentTransactionsAdmin(admin.ModelAdmin):
    list_display = ('user', 'package', 'create', 'result_code', 'tarif')
admin.site.register(PaymentTransactions, PaymentTransactionsAdmin)



class UsersPackagesAdmin(admin.ModelAdmin):
    list_display = ('user', 'package', 'auto_subscribe', 'subscription_date', 'expire_date')
admin.site.register(UsersPackages, UsersPackagesAdmin)








