# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe

# Create your models here.


class Tarif(models.Model):
    name = models.CharField(max_length=45, blank=True)
    price = models.FloatField(blank=True, null=True)
    def __unicode__(self):
        return self.name

class Packages(models.Model):
    tarif = models.ForeignKey('Tarif', blank=True, null=True)
    name = models.CharField(max_length=45, blank=True)
    logo = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    priority = models.IntegerField(blank=True, null=True)
    enabled = models.NullBooleanField()
    def __unicode__(self):
        return self.name
    def thumb(self):
        return mark_safe('<img src="%s" width="150">' % self.logo)
    class Meta:
        db_table = 'packages'
        verbose_name_plural = 'Packages'
        verbose_name = 'package'

class Programs(models.Model):
    name = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=200, blank=True)
    begin_t = models.DateTimeField(blank=True, null=True)
    end_t = models.DateTimeField(blank=True, null=True)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'programs'
        verbose_name_plural = 'Video programs'
        verbose_name = 'program'

class Channels(models.Model):
    name = models.CharField(max_length=45, blank=True)
    country = models.CharField(max_length=45, blank=True)
    stream = models.CharField(max_length=45, blank=True)
    application = models.CharField(max_length=45, blank=True)
    server = models.CharField(max_length=45, blank=True)
    logo = models.CharField(max_length=200, blank=True)
    screenshot = models.CharField(max_length=200, blank=True)
    is_free = models.BooleanField()
    package = models.ForeignKey(Packages, blank=True, null=True)
    programs = models.ManyToManyField(Programs,blank=True,)
    def thumb(self):
        return mark_safe('<img src="%s" width="150">' % self.logo)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'channels'
        verbose_name_plural = 'Video channels'
        verbose_name = 'channel'


class PaymentTransactions(models.Model):
    TYPE_CODE = (
                 ('0', _(u'pending')),
                ('100', _(u'Ok')),
                ('400', _(u'Wrong or Not existing number')),
                ('401', _(u'Not enough money')),
                ('402', _(u'Wrong tariff')),
                ('403', _(u'Service Denied')),
                ('404', _(u'Subscriber Not Active')),
                ('499', _(u'Subscriber Ported Out')),
                ('500', _(u'Internal Error')),
             )
    user = models.ForeignKey('Users', blank=True, null=True,verbose_name=_(u'User'))
    package = models.ForeignKey(Packages, blank=True, null=True)
    create = models.DateTimeField(auto_now_add=True,
                                  verbose_name=_(u'Created at'))
    result_code = models.CharField(max_length=20, verbose_name=_(u"Response code"), choices=TYPE_CODE,default='pending')
    tarif = models.ForeignKey(Tarif, blank=True, null=True, verbose_name=_(u'Tarif'))
    class Meta:
        db_table = 'payment_transactions'

class ProgramManager(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=45, blank=True)
    login = models.CharField(max_length=45, blank=True)
    password = models.CharField(max_length=45, blank=True)
    class Meta:
        managed = False
        db_table = 'program_manager'

class ProgramManagerChannels(models.Model):
    program_m = models.ForeignKey(ProgramManager)
    channel = models.ForeignKey(Channels)
    class Meta:
        managed = False
        db_table = 'program_manager_channels'



class TvSchedule(models.Model):
    program = models.ForeignKey(Programs, blank=True, null=True)
    begin_t = models.DateTimeField(blank=True, null=True)
    end_t = models.DateTimeField(blank=True, null=True)
    class Meta:
        db_table = 'tv_schedule'

class Users(models.Model):
    uid = models.CharField(unique=True, max_length=200, blank=True)
    msisdn = models.CharField(max_length=20)
    subscription_expire_date = models.DateTimeField(blank=True, null=True)
    auto_subscribe = models.BooleanField()
    def __unicode__(self):
        return self.uid
    class Meta:
        db_table = 'users'

class UsersPackages(models.Model):
    user = models.ForeignKey(Users)
    package = models.ForeignKey(Packages)
    auto_subscribe = models.BooleanField()
    subscription_date = models.DateTimeField(blank=True, null=True)
    expire_date = models.DateTimeField(blank=True, null=True)
    class Meta:
        db_table = 'users_packages'




