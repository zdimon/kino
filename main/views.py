# -*- coding: utf8 -*-
__author__ = 'km'
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
import os
from main.models import  Users, Channels, Packages, PaymentTransactions, UsersPackages
import json
import time
#Charging Gateway IP: 10.220.2.217
# > Allowed tariffs: 43 and 37(Tariff143 and Tariff137)
from config.settings import CHARGING_GATEWAY
from datetime import datetime, timedelta
from django.utils.translation import ugettext as _


def home(request):
    return HttpResponseRedirect('/admin')
    #return HttpResponse("<h1>Hello!</h1>")


def ping(request):
    return HttpResponse("OK")


def auth(request,msisdn,uid):
    try:
        user = Users.objects.filter(uid=uid, msisdn=msisdn).get()
    except:
        user = Users()
        user.uid = uid
        user.msisdn = msisdn
        user.auto_subscribe = False
        user.save()

    ts = int(time.time())
    response_data = {}
    try:
    	te = int(time.mktime(user.subscription_expire_date.timetuple()))
    except:
        te = 0
    response_data['mbsServer'] = {"version": 1, "serverTime": ts, "status": 2000, "subscriptionExpireTime": te}
    #response_data['message'] = 'You messed up'
    return HttpResponse(json.dumps(response_data), content_type="application/json")



def channels(request,uid):
    try:
        user = Users.objects.filter(uid=uid).get()
    except:
        return HttpResponse('Error. User not defined.')
    ch = Channels.objects.all()
    cl = []
    for c in ch:
        try:
            userp = UsersPackages.objects.filter(user=user,package=c.package).get()
            exp = int(time.mktime(userp.expire_date.timetuple()))
            nt = int(time.time())
            diff = exp - nt
            if diff > 0:
                access = 'true'
            else:
                access ='false'
            #import pdb; pdb.set_trace()
        except:
            access ='false'
        if c.is_free:
            access ='true'
        cl.append({"id":c.id, "name": c.name, "logo": c.logo, "country": c.country, "access": c.is_free, "package": c.package_id})
    response_data = {}
    ts = int(time.time())
    try:
    	te = int(time.mktime(user.subscription_expire_date.timetuple()))
    except:
        te = 0
    response_data['mbsServer'] = {"version": 1, "serverTime": ts, "status": 2000, "subscriptionExpireTime": te, "channels": cl}
    #response_data['message'] = 'You messed up'
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def channel(request,id,uid):
    try:
        user = Users.objects.filter(uid=uid).get()
    except:
        return HttpResponse('Error. User not defined.')
    try:
        ch = Channels.objects.get(pk=id)
    except:
        return HttpResponse('Error. Chanel is not defined.')
    #cl = []
    #for c in ch:
    pr = []
    for p in ch.programs.all():
        pr.append({"id": p.pk, "name": p.name, "description": p.description, "begin_t": int(time.mktime(p.begin_t.timetuple())), "end_t": int(time.mktime(p.end_t.timetuple()))})
    cl = {"id": ch.id, "name": ch.name, "logo": ch.logo, "screenshot": ch.screenshot, "viewers": 0,  "access": ch.is_free, "packageId": ch.package_id, "programs": pr}
    response_data = {}
    ts = int(time.time())
    try:
    	te = int(time.mktime(user.subscription_expire_date.timetuple()))
    except:
        te = 0
    response_data['mbsServer'] = {"version": 1, "serverTime": ts, "status": 2000, "subscriptionExpireTime": te, "channel": cl}
    #response_data['message'] = 'You messed up'
    return HttpResponse(json.dumps(response_data), content_type="application/json")



def streems(request,id,uid):
    #import pdb; pdb.set_trace()
    try:
        user = Users.objects.filter(uid=uid).get()
    except:
        return HttpResponse('Error. User not defined.')
    try:
        ch = Channels.objects.get(pk=id)
    except:
        return HttpResponse('Error. Chanel is not defined.')
    b200hls = "http://d.edge.acell.mbsys.tv:1935/%s/200_%s/playlist.m3u8" % (ch.application, ch.stream)
    b200rtsp = "http://d.edge.acell.mbsys.tv:1935/%s/200_%s" % (ch.application, ch.stream)
    b500hls = "http://d.edge.acell.mbsys.tv:1935/%s/500_%s/playlist.m3u8" % (ch.application, ch.stream)
    b500rtsp = "http://d.edge.acell.mbsys.tv:1935/%s/500_%s" % (ch.application, ch.stream)
    strm = [{"birate": 200, "hls": b200hls, "rtsp": b200rtsp}, {"birate": 500, "hls": b500hls, "rtsp": b500rtsp}]
    cl = {"id": ch.id, "name": ch.name, "logo": ch.logo, "screenshot": ch.screenshot, "viewers": 0,  "access": ch.is_free, "packageId": ch.package_id, "streams": strm}
    response_data = {}
    ts = int(time.time())
    try:
    	te = int(time.mktime(user.subscription_expire_date.timetuple()))
    except:
        te = 0
    response_data['mbsServer'] = {"version": 1, "serverTime": ts, "status": 2000, "subscriptionExpireTime": te, "channel": cl}
    #response_data['message'] = 'You messed up'
    return HttpResponse(json.dumps(response_data), content_type="application/json")



def packages(request,uid):
    try:
        user = Users.objects.filter(uid=uid).get()
    except:
        return HttpResponse('Error. User not defined.')

    pr = []
    for p in Packages.objects.all():
        channels = []
        for c in p.channels_set.all():
            channels.append({"id": c.pk, "name": c.name, "logo": c.logo, "country": c.country})
        try:
            userp = UsersPackages.objects.filter(user=user,package=p).get()
            exp = int(time.mktime(userp.expire_date.timetuple()))
            nt = int(time.time())
            diff = exp - nt
            if diff > 0:
                days = diff/86400
            else:
                days = 0
            #import pdb; pdb.set_trace()
        except:
            days = 0
            exp = 0
        pr.append({"id": p.pk, "name": p.name, "description": p.description, "logo": p.logo, "price": p.tarif.price, "priority": p.priority, "channels": channels, "expire": days})

    response_data = {}
    ts = int(time.time())
    try:
        te = int(time.mktime(user.subscription_expire_date.timetuple()))
    except:
        te = 0
    response_data['mbsServer'] = {"version": 1, "serverTime": ts, "status": 2000, "subscriptionExpireTime": te, "packages": pr}
    #response_data['message'] = 'You messed up'
    return HttpResponse(json.dumps(response_data), content_type="application/json")



def save_client_info(request):
    try:
        msisdn = request.GET['msisdn']
    except:
        return HttpResponse('<response status="1001"  message="Error. There is no msisdn parameter." />')

    try:
        tarif = request.GET['tarif']
    except:
        return HttpResponse('<response status="1002"  message="Error. There is no tarif parameter." />')

    try:
        m = MobileDevices.objects.filter(msisdn = msisdn).get()
        return HttpResponse('<response status="1002"  message="Error. Device with this msisdn is already exists." />')
    except:
        pass

    m = MobileDevices()
    m.msisdn = msisdn
    m.tarif = tarif
    m.save()
    return HttpResponse('<response status="0"  message="Success" />')

def test(request):
    import random
    d = {'100':'100', '400':'400', '401':'401'}
    return HttpResponse(random.choice(d.keys()))


def init_charging(request,package_id,msisdn,uid):
    from django.utils.timezone import utc
    try:
        user = Users.objects.filter(uid=uid).get()
    except:
        return HttpResponse('Error. User not defined.')

    try:
        package = Packages.objects.filter(pk=package_id).get()
    except:
        return HttpResponse('Error. Packet id is not defined.')

    import urllib2
    if msisdn == 0:
        urlstr = CHARGING_GATEWAY+'?msisdn='+user.msisdn+'&tarif='+package.tarif.name
    else:
        urlstr = CHARGING_GATEWAY+'?msisdn='+msisdn+'&tarif='+package.tarif.name
    #import pdb; pdb.set_trace()
    try:
        p = urllib2.urlopen(urlstr).read()
        print 'make request '+ urlstr
        t = PaymentTransactions()
        t.user = user
        t.package = package
        t.result_code = p
        t.tarif = package.tarif
        t.save()
        if p == '100':
            #import pdb; pdb.set_trace()
            try:
                up = UsersPackages.objects.filter(user=user, package=package).get()
                cd = up.expire_date
            except:
                up = UsersPackages()
                up.user = user
                up.auto_subscribe = False
                up.package = package
                cd = datetime.utcnow().replace(tzinfo=utc)
            now = datetime.utcnow().replace(tzinfo=utc)
            exp = cd+timedelta(days=30)
            up.subscription_date = now
            up.expire_date = exp
            up.save()
        response_data = {}
        ts = int(time.time())
        try:
            te = int(time.mktime(user.subscription_expire_date.timetuple()))
        except:
            te = 0
        TYPE_CODE = {
                '100': _(u'Ok'),
                '400': _(u'Wrong or Not existing number'),
                '401': _(u'Not enough money'),
                '402': _(u'Wrong tariff'),
                '403': _(u'Service Denied'),
                '404': _(u'Subscriber Not Active'),
                '499': _(u'Subscriber Ported Out'),
                '500': _(u'Internal Error'),
                }
        pr = {"code": p, "description": TYPE_CODE[p]}
        response_data['mbsServer'] = {"version": 1, "serverTime": ts, "status": p, "subscriptionExpireTime": te}
        #import pdb; pdb.set_trace()
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except Exception as e:
        return HttpResponse(e)
        return HttpResponse(urlstr)
