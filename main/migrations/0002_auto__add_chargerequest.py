# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ChargeRequest'
        db.create_table(u'main_chargerequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('msisdn', self.gf('django.db.models.fields.CharField')(max_length=100, db_index=True)),
            ('parameters', self.gf('django.db.models.fields.CharField')(max_length=250, db_index=True)),
            ('result_code', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('responce_body', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['ChargeRequest'])


    def backwards(self, orm):
        # Deleting model 'ChargeRequest'
        db.delete_table(u'main_chargerequest')


    models = {
        u'main.chargerequest': {
            'Meta': {'object_name': 'ChargeRequest'},
            'create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '250', 'db_index': 'True'}),
            'responce_body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'result_code': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'main.mobiledevices': {
            'Meta': {'object_name': 'MobileDevices'},
            'create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'tarif': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['main']