# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing M2M table for field channels on 'Packages'
        db.delete_table(db.shorten_name('packages_channels'))

        # Adding field 'Channels.package'
        db.add_column('channels', 'package',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Packages'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding M2M table for field channels on 'Packages'
        m2m_table_name = db.shorten_name('packages_channels')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('packages', models.ForeignKey(orm[u'main.packages'], null=False)),
            ('channels', models.ForeignKey(orm[u'main.channels'], null=False))
        ))
        db.create_unique(m2m_table_name, ['packages_id', 'channels_id'])

        # Deleting field 'Channels.package'
        db.delete_column('channels', 'package_id')


    models = {
        u'main.channels': {
            'Meta': {'object_name': 'Channels', 'db_table': "'channels'"},
            'application': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free': ('django.db.models.fields.BooleanField', [], {}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Packages']", 'null': 'True', 'blank': 'True'}),
            'screenshot': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'server': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'stream': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        u'main.chargerequest': {
            'Meta': {'object_name': 'ChargeRequest'},
            'create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '250', 'db_index': 'True'}),
            'responce_body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'result_code': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '20'})
        },
        u'main.mobiledevices': {
            'Meta': {'object_name': 'MobileDevices'},
            'create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'tarif': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.packages': {
            'Meta': {'object_name': 'Packages', 'db_table': "'packages'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'enabled': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.paymenttransactions': {
            'Meta': {'object_name': 'PaymentTransactions', 'db_table': "'payment_transactions'", 'managed': 'False'},
            'amount': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'op_transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Packages']", 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'status_code': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'status_description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Users']", 'null': 'True', 'blank': 'True'})
        },
        u'main.programmanager': {
            'Meta': {'object_name': 'ProgramManager', 'db_table': "'program_manager'", 'managed': 'False'},
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        u'main.programmanagerchannels': {
            'Meta': {'object_name': 'ProgramManagerChannels', 'db_table': "'program_manager_channels'", 'managed': 'False'},
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Channels']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'program_m': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.ProgramManager']"})
        },
        u'main.programs': {
            'Meta': {'object_name': 'Programs', 'db_table': "'programs'"},
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Channels']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'main.tvschedule': {
            'Meta': {'object_name': 'TvSchedule', 'db_table': "'tv_schedule'"},
            'begin_t': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'end_t': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'program': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Programs']", 'null': 'True', 'blank': 'True'})
        },
        u'main.users': {
            'Meta': {'object_name': 'Users', 'db_table': "'users'"},
            'auto_subscribe': ('django.db.models.fields.BooleanField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'subscription_expire_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'})
        },
        u'main.userspackages': {
            'Meta': {'object_name': 'UsersPackages', 'db_table': "'users_packages'", 'managed': 'False'},
            'auto_subscribe': ('django.db.models.fields.BooleanField', [], {}),
            'expire_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Packages']"}),
            'subscription_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Users']"})
        }
    }

    complete_apps = ['main']