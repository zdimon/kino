# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'MobileDevices'
        db.delete_table(u'main_mobiledevices')

        # Deleting model 'ChargeRequest'
        db.delete_table(u'main_chargerequest')


    def backwards(self, orm):
        # Adding model 'MobileDevices'
        db.create_table(u'main_mobiledevices', (
            ('msisdn', self.gf('django.db.models.fields.CharField')(max_length=100, db_index=True)),
            ('tarif', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'main', ['MobileDevices'])

        # Adding model 'ChargeRequest'
        db.create_table(u'main_chargerequest', (
            ('parameters', self.gf('django.db.models.fields.CharField')(max_length=250, db_index=True)),
            ('msisdn', self.gf('django.db.models.fields.CharField')(max_length=100, db_index=True)),
            ('create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('responce_body', self.gf('django.db.models.fields.TextField')(blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('result_code', self.gf('django.db.models.fields.CharField')(default='pending', max_length=20)),
        ))
        db.send_create_signal(u'main', ['ChargeRequest'])


    models = {
        u'main.channels': {
            'Meta': {'object_name': 'Channels', 'db_table': "'channels'"},
            'application': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free': ('django.db.models.fields.BooleanField', [], {}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Packages']", 'null': 'True', 'blank': 'True'}),
            'programs': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['main.Programs']", 'symmetrical': 'False'}),
            'screenshot': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'server': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'stream': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        u'main.packages': {
            'Meta': {'object_name': 'Packages', 'db_table': "'packages'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'enabled': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tarif': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Tarif']", 'null': 'True', 'blank': 'True'})
        },
        u'main.paymenttransactions': {
            'Meta': {'object_name': 'PaymentTransactions', 'db_table': "'payment_transactions'"},
            'create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Packages']", 'null': 'True', 'blank': 'True'}),
            'result_code': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '20'}),
            'tarif': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Tarif']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Users']", 'null': 'True', 'blank': 'True'})
        },
        u'main.programmanager': {
            'Meta': {'object_name': 'ProgramManager', 'db_table': "'program_manager'", 'managed': 'False'},
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        u'main.programmanagerchannels': {
            'Meta': {'object_name': 'ProgramManagerChannels', 'db_table': "'program_manager_channels'", 'managed': 'False'},
            'channel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Channels']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'program_m': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.ProgramManager']"})
        },
        u'main.programs': {
            'Meta': {'object_name': 'Programs', 'db_table': "'programs'"},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'main.tarif': {
            'Meta': {'object_name': 'Tarif'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        u'main.tvschedule': {
            'Meta': {'object_name': 'TvSchedule', 'db_table': "'tv_schedule'"},
            'begin_t': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'end_t': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'program': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Programs']", 'null': 'True', 'blank': 'True'})
        },
        u'main.users': {
            'Meta': {'object_name': 'Users', 'db_table': "'users'"},
            'auto_subscribe': ('django.db.models.fields.BooleanField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            'subscription_expire_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200', 'blank': 'True'})
        },
        u'main.userspackages': {
            'Meta': {'object_name': 'UsersPackages', 'db_table': "'users_packages'"},
            'auto_subscribe': ('django.db.models.fields.BooleanField', [], {}),
            'expire_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Packages']"}),
            'subscription_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Users']"})
        }
    }

    complete_apps = ['main']