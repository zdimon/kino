# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MobileDevices'
        db.create_table(u'main_mobiledevices', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('msisdn', self.gf('django.db.models.fields.CharField')(max_length=100, db_index=True)),
            ('tarif', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('create', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['MobileDevices'])


    def backwards(self, orm):
        # Deleting model 'MobileDevices'
        db.delete_table(u'main_mobiledevices')


    models = {
        u'main.mobiledevices': {
            'Meta': {'object_name': 'MobileDevices'},
            'create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'msisdn': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'tarif': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['main']