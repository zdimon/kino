from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

#http://localhost:8000/azercell-server/api/auth/068745362/uid-uid-uid
#http://localhost:8000/azercell-server/api/channels/uid-uid-uid
#http://localhost:8000/azercell-server/api/channel/1/uid-uid-uid
#http://localhost:8000/azercell-server/api/channel/1/streams/uid-uid-uid
#http://localhost:8000/azercell-server/api/packages/uid-uid-uid
#http://localhost:8000/azercell-server/api/request/charging/21/uid-uid-uid


urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'main.views.home', name='home'),
    url(r'^save_client_info$', 'main.views.save_client_info', name='save_client_info'),
    url(r'^azercell-server/api/request/charging/(?P<package_id>\w+)/(?P<msisdn>[-\w]+)/(?P<uid>[-\w]+)', 'main.views.init_charging', name='init_charging'),

    url(r'^test', 'main.views.test', name='test'),
    url(r'^ping', 'main.views.ping', name='ping'),
    url(r'^azercell-server/api/auth/(?P<msisdn>\w+)/(?P<uid>[-\w]+)$', 'main.views.auth', name='auth'),
    url(r'^azercell-server/api/channels/(?P<uid>[-\w]+)', 'main.views.channels', name='channels'),
    url(r'^azercell-server/api/channel/(?P<id>\w+)/(?P<uid>[-\w]+)$', 'main.views.channel', name='channel'),
    url(r'^azercell-server/api/channel/(?P<id>\w+)/streams/(?P<uid>[-\w]+)$', 'main.views.streems', name='streems'),
    url(r'^azercell-server/api/packages/(?P<uid>[-\w]+)$', 'main.views.packages', name='packages'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
)
